# pvpgn-tracker
A daemon to transform PvPGN solicit requests over UDP to HTTP API calls.

The HTTP API service can be found at
[pvpgn-tracker-web](https://github.com/BNETDocs/pvpgn-tracker-web).
